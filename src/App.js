import React from 'react';
import './App.css';
import Booking from './components/Booking';
import Intro from './components/Intro';
import ContextBook from './components/ContextBook';

function App() {
  return (
    <div className="App">
      <Intro/>
      {/* <Booking/> */}
      <ContextBook/>
    </div>
  );
}

export default App;

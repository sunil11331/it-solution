import React from 'react';
import { createContext } from 'react';
import '../App.css';
import Menu from './Service';
import ContextBook from './ContextBook';

export const BookContext = createContext();
const Booking = () => {

  return (
      <>
        <BookContext.Provider value={Menu}>
           <ContextBook/>
        </BookContext.Provider>
      </>
  )
}

export default Booking
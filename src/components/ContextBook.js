import React from 'react'
import { useContext, useState } from 'react';
import Index from './Index';
import Menu from './Service';
import '../App.css';
import { useEffect } from 'react';
/* import { BookContext } from './Booking'; */
let temp=undefined
const ContextBook = (props) => {

    const [item, setItem] = useState(Menu);
    let [count, setCount] = useState(60);
    /* let [cart, setCart] = useState(0) */
   /*  function runText (){
        document.getElementById("mydiv".textContent += 1)
    }
    setInterval(runText, 1000); */
    
    useEffect(()=>{
      console.log('wolf')
      if(temp) return
      temp=setInterval(() => {
        count-=1
        setCount(count)
      }, 1000);
    }, [])

    

    /* const item = useContext(BookContext); */
  return (
    <>
      <div className='content'>
        <div className='container'>
          <div className='continue-booking'>
            <p id='mydiv'>Time Left: {count} seconds</p>
            <h1>Claim Your Free Trial Class<span><i className="bi bi-cart"></i></span>0</h1>
          </div>
          {/* <div className='cart'>
          <i class="bi bi-cart"></i>
          <span>0</span>
          </div> */}
          <div className='class-schedule'>
            <h1>Class Schedule</h1>
            <p>Free Seats Left: <span style={{color:"orangered"}}>7</span></p>
          </div>
        <section className='box'>
            <div className='heading'>
                <p>Date</p>
                <p>Time</p>
                <p>Availability</p>
            </div>
            {
                item.map((curItem) =>{
                    return <Index key={curItem.id} {...curItem}/>
                })
            }
            
        </section>

    </div>
    </div> 
    </>
  )
}

export default ContextBook

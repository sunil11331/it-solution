import React from 'react'
import { useState } from 'react';
import '../App.css';

const Index = ({date, time, avail}) => {

  const [cart, setCart] = useState(0)
  return (
    <div className='ind'>
        
            <div className='student-container'>
                <div className='date-info'>
                    <p>{date}</p>
                </div>
                <div className='time-info'>
                    <p>{time}</p>
                </div>
                <div className='availability-info'>
                    <p>{avail}</p>
                </div>
                <button className='btn' onClick={() => setCart(cart+1)}>submit</button>
                
            </div>
    </div>
  )
}

export default Index;
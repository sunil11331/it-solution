import React from 'react';
import { useState } from 'react';
import '../App.css'

const Intro = () => {
  const [place, setPlace] = useState(false)
  
  return (
    <div className='content'>
        <div className='container-1'>
        <h1>## These classes are scheduled for the same days every month, 
                generate the schedule for next 60 days</h1>
            <div className='header'>
                <div className='header-1'>
                  <i class="bi bi-file-ruled"></i>
                  <a href='#'>Default view</a>
                </div>
                <ul className='right-header'>
                  <li><a href='#'>filter</a></li>
                  <li><a href='#'>sort</a></li>
                  <li><i onClick={()=>{setPlace(true("enter your text"))}} class="bi bi-search"></i></li>
                  <li><i class="bi bi-arrows-angle-expand"></i></li>
                </ul>
            </div>
            <hr/>
            <h1 style={{color:"black", fontSize:"20px", fontWeight:"600"}}>Class Schedule</h1>
            <div className='app-container'>
            <table>
              <thead className='head'>
                <tr style={{color:"gray", padding:"10px"}}>
                  <td><u>Aa </u>Name</td>
                  <td><i class="bi bi-sliders"></i> Week Days</td>
                  <td><i class="bi bi-sliders"></i> Time for May 2021</td>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Python</td>{/* <button>open</button> */}
                  <td>Every Monday</td>
                  <td>4pm</td>
                </tr>
                <tr>
                  <td>Java</td>
                  <td> Every Wednesday</td>
                  <td>5pm</td>
                </tr>
                <tr>
                  <td>Html</td>
                  <td>Every Friday and Saturday</td>
                  <td>9am</td>
                </tr>
              </tbody>
            </table>
            <div className='count'>
              COUNT <span style={{fontSize:"16px"}}>3</span>
            </div>
            {<h1 style={{color:"rgb(42 41 41)", fontSize:"30px",margin:"0"}}>Online Class Booking UI</h1>}
            </div>
        </div>
    </div>
  )
}

export default Intro